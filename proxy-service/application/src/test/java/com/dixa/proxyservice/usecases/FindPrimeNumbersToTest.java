package com.dixa.proxyservice.usecases;

import com.dixa.proxyservice.ports.PrimeNumbersGateway;



import java.util.*;


public class FindPrimeNumbersToTest {
    private final static List<Integer> FIRST_100_PRIME_NUMBERS = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97);
    PrimeNumbersGateway<Integer> primeNumbersGateway;
    FindPrimeNumbersTo<Integer> findPrimeNumbersTo;

    public void init() {
        findPrimeNumbersTo = new FindPrimeNumbersTo<>(primeNumbersGateway);
        List<Integer> emptyList = new ArrayList<>();
        List<Integer> oneElementList = Collections.singletonList(2);
        //Mockito.lenient().when(primeNumbersGateway.calculatePrimeNumbersTo(1)).thenReturn(emptyList.iterator());
        //Mockito.lenient().when(primeNumbersGateway.calculatePrimeNumbersTo(2)).thenReturn(oneElementList.iterator());
        //Mockito.lenient().when(primeNumbersGateway.calculatePrimeNumbersTo(100)).thenReturn(FIRST_100_PRIME_NUMBERS.iterator());

    }


    public void shouldReturnEmptyList() {
        int toNumber = 1;
        Iterator<Integer> result = this.findPrimeNumbersTo.execute(toNumber);
        //assertNotNull(result);
        //assertFalse(result.hasNext());

    }

    public void shouldReturnOneElementList() {
        int toNumber = 2;
        List<Integer> expectedResult = Collections.singletonList(2);
        Iterator<Integer> result = this.findPrimeNumbersTo.execute(toNumber);
        List<Integer> resultList = new ArrayList<Integer>();
        result.forEachRemaining(resultList::add);
        //assertEquals(resultList, expectedResult);
    }

    public void shouldReturnFirst100PrimeNumbers() {
        int toNumber = 100;
        Iterator<Integer> result = this.findPrimeNumbersTo.execute(toNumber);
        List<Integer> resultList = new ArrayList<Integer>();
        result.forEachRemaining(resultList::add);
        //assertEquals(resultList, FIRST_100_PRIME_NUMBERS);
    }
}
