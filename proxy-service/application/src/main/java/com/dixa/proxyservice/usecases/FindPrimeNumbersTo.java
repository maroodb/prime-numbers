package com.dixa.proxyservice.usecases;
import com.dixa.proxyservice.ports.PrimeNumbersGateway;
import java.util.Iterator;

public class FindPrimeNumbersTo <T> {

    private final PrimeNumbersGateway<T> primeNumbersGateway;

    public FindPrimeNumbersTo(PrimeNumbersGateway<T> primeNumbersGateway) {
        this.primeNumbersGateway = primeNumbersGateway;
    }

    public Iterator<T> execute(int toNumber) {
        return this.primeNumbersGateway.calculatePrimeNumbersTo(toNumber);
    }
}
