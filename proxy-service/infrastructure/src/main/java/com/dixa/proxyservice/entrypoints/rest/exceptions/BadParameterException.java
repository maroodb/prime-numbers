package com.dixa.proxyservice.entrypoints.rest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Provided number is null or negative")
public class BadParameterException extends RuntimeException {
}
