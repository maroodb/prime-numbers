package com.dixa.proxyservice.config;

import com.dixa.grpc.generated.NextPrimeNumbersResponse;
import com.dixa.proxyservice.ports.PrimeNumbersGateway;
import com.dixa.proxyservice.usecases.FindPrimeNumbersTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
public class ContainerConfig {

    private final PrimeNumbersGateway<NextPrimeNumbersResponse> primeNumbersGateway;

    @Autowired
    public ContainerConfig(PrimeNumbersGateway<NextPrimeNumbersResponse> primeNumbersGateway) {
        this.primeNumbersGateway = primeNumbersGateway;
    }

    @Bean
    public FindPrimeNumbersTo<NextPrimeNumbersResponse> findPrimeNumbersTo(PrimeNumbersGateway<NextPrimeNumbersResponse> primeNumbersGateway) {
        return new FindPrimeNumbersTo<NextPrimeNumbersResponse>(primeNumbersGateway);
    }

}
