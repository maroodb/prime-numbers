package com.dixa.proxyservice;

import com.dixa.proxyservice.server.WebServer;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        WebServer webServer = new WebServer();
        webServer.start(args);
    }
}
