package com.dixa.proxyservice.adapters;

import com.dixa.grpc.generated.*;
import com.dixa.proxyservice.config.AppConfig;
import com.dixa.proxyservice.ports.PrimeNumbersGateway;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.stereotype.Component;
import java.util.Iterator;
@Component
public class PrimeNumbersGrpcGateway implements PrimeNumbersGateway <NextPrimeNumbersResponse> {


    private final int GATEWAY_PORT = AppConfig.getInstance().getGatewayPort();
    private final static String GATEWAY_HOST = AppConfig.getInstance().getGatewayHost();


    ManagedChannel channel = ManagedChannelBuilder.forAddress(GATEWAY_HOST, GATEWAY_PORT).usePlaintext().build();

    PrimeNumbersServiceStreamGrpc.PrimeNumbersServiceStreamBlockingStub client
            = PrimeNumbersServiceStreamGrpc.newBlockingStub(channel);

    @Override
    public Iterator<NextPrimeNumbersResponse> calculatePrimeNumbersTo(int toNumber) {
        PrimeNumbersRequest request = PrimeNumbersRequest.newBuilder().setToNumber(toNumber).build();
        return client.calculatePrimeNumbers(request);
    }
}
