package com.dixa.proxyservice.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppConfig {

    private final String DEFAULT_GATEWAY_HOST = "localhost";
    private final int DEFAULT_GATEWAY_PORT = 8000;
    private static AppConfig instance;
    private Properties properties;

    private AppConfig() {
        this.properties = new Properties();
        try (InputStream input = getClass().getClassLoader().getResourceAsStream("application.properties");){
            this.properties.load(input);
            System.out.println(properties);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public int getGatewayPort() {
        String port = this.properties.getProperty("grpc.gateway.port");
        if (port != null) {
            return Integer.parseInt(port);
        }
        return DEFAULT_GATEWAY_PORT;
    }

    public String getGatewayHost() {
        String host = this.properties.getProperty("grpc.gateway.host");
        return host != null ? host : DEFAULT_GATEWAY_HOST;
    }
    public static AppConfig getInstance() {
        if (instance == null) {
            instance = new AppConfig();
        }
        return instance;
    }
}
