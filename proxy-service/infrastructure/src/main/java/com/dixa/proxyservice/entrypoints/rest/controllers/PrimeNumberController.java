package com.dixa.proxyservice.entrypoints.rest.controllers;
import com.dixa.proxyservice.entrypoints.rest.exceptions.BadParameterException;
import com.dixa.proxyservice.entrypoints.rest.services.PrimeNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;


@RestController
@RequestMapping("/prime")
public class PrimeNumberController {

    private final PrimeNumberService primeNumberService;
    @Autowired
    public PrimeNumberController(PrimeNumberService primeNumberService) {
        this.primeNumberService = primeNumberService;
    }

    @GetMapping(value = "/{toNumber}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Integer> getPrimeNumbers(@PathVariable("toNumber") Integer toNumber) {
        if(toNumber == null || toNumber < 0) {
            throw new BadParameterException();
        }
        return primeNumberService.getPrimeNumbers(toNumber);
    }
}
