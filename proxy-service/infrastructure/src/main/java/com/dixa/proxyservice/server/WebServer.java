package com.dixa.proxyservice.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.dixa.proxyservice")
public class WebServer {
    public void start(String [] args) {
        SpringApplication.run(WebServer.class, args);
    }
}
