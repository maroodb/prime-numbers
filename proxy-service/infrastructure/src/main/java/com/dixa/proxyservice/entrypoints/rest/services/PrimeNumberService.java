package com.dixa.proxyservice.entrypoints.rest.services;

import com.dixa.grpc.generated.NextPrimeNumbersResponse;
import com.dixa.proxyservice.usecases.FindPrimeNumbersTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class PrimeNumberService {

    private final FindPrimeNumbersTo<NextPrimeNumbersResponse> findPrimeNumbersTo;

    @Autowired
    public PrimeNumberService(FindPrimeNumbersTo<NextPrimeNumbersResponse> findPrimeNumbersTo) {
        this.findPrimeNumbersTo = findPrimeNumbersTo;
    }
    public Flux<Integer> getPrimeNumbers(int toNumber) {
        return Flux.create(integerFluxSink -> {
            this.findPrimeNumbersTo.execute(toNumber)
                    .forEachRemaining(output -> {
                        integerFluxSink.next(output.getNumber());
                    });
            integerFluxSink.complete();
        });
    }
}
