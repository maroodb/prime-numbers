// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: primeNumbers.proto

package com.dixa.grpc.generated;

/**
 * Protobuf type {@code com.dixa.grpc.generated.PrimeNumbersRequest}
 */
public  final class PrimeNumbersRequest extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:com.dixa.grpc.generated.PrimeNumbersRequest)
    PrimeNumbersRequestOrBuilder {
private static final long serialVersionUID = 0L;
  // Use PrimeNumbersRequest.newBuilder() to construct.
  private PrimeNumbersRequest(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private PrimeNumbersRequest() {
    toNumber_ = 0;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private PrimeNumbersRequest(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 8: {

            toNumber_ = input.readInt32();
            break;
          }
          default: {
            if (!parseUnknownFieldProto3(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.dixa.grpc.generated.PrimeNumbers.internal_static_com_dixa_grpc_generated_PrimeNumbersRequest_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.dixa.grpc.generated.PrimeNumbers.internal_static_com_dixa_grpc_generated_PrimeNumbersRequest_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.dixa.grpc.generated.PrimeNumbersRequest.class, com.dixa.grpc.generated.PrimeNumbersRequest.Builder.class);
  }

  public static final int TONUMBER_FIELD_NUMBER = 1;
  private int toNumber_;
  /**
   * <code>int32 toNumber = 1;</code>
   */
  public int getToNumber() {
    return toNumber_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (toNumber_ != 0) {
      output.writeInt32(1, toNumber_);
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (toNumber_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt32Size(1, toNumber_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.dixa.grpc.generated.PrimeNumbersRequest)) {
      return super.equals(obj);
    }
    com.dixa.grpc.generated.PrimeNumbersRequest other = (com.dixa.grpc.generated.PrimeNumbersRequest) obj;

    boolean result = true;
    result = result && (getToNumber()
        == other.getToNumber());
    result = result && unknownFields.equals(other.unknownFields);
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + TONUMBER_FIELD_NUMBER;
    hash = (53 * hash) + getToNumber();
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.dixa.grpc.generated.PrimeNumbersRequest parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.dixa.grpc.generated.PrimeNumbersRequest parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.dixa.grpc.generated.PrimeNumbersRequest parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.dixa.grpc.generated.PrimeNumbersRequest parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.dixa.grpc.generated.PrimeNumbersRequest parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.dixa.grpc.generated.PrimeNumbersRequest parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.dixa.grpc.generated.PrimeNumbersRequest parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.dixa.grpc.generated.PrimeNumbersRequest parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.dixa.grpc.generated.PrimeNumbersRequest parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.dixa.grpc.generated.PrimeNumbersRequest parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.dixa.grpc.generated.PrimeNumbersRequest parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.dixa.grpc.generated.PrimeNumbersRequest parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.dixa.grpc.generated.PrimeNumbersRequest prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code com.dixa.grpc.generated.PrimeNumbersRequest}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:com.dixa.grpc.generated.PrimeNumbersRequest)
      com.dixa.grpc.generated.PrimeNumbersRequestOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.dixa.grpc.generated.PrimeNumbers.internal_static_com_dixa_grpc_generated_PrimeNumbersRequest_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.dixa.grpc.generated.PrimeNumbers.internal_static_com_dixa_grpc_generated_PrimeNumbersRequest_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.dixa.grpc.generated.PrimeNumbersRequest.class, com.dixa.grpc.generated.PrimeNumbersRequest.Builder.class);
    }

    // Construct using com.dixa.grpc.generated.PrimeNumbersRequest.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      toNumber_ = 0;

      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.dixa.grpc.generated.PrimeNumbers.internal_static_com_dixa_grpc_generated_PrimeNumbersRequest_descriptor;
    }

    @java.lang.Override
    public com.dixa.grpc.generated.PrimeNumbersRequest getDefaultInstanceForType() {
      return com.dixa.grpc.generated.PrimeNumbersRequest.getDefaultInstance();
    }

    @java.lang.Override
    public com.dixa.grpc.generated.PrimeNumbersRequest build() {
      com.dixa.grpc.generated.PrimeNumbersRequest result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public com.dixa.grpc.generated.PrimeNumbersRequest buildPartial() {
      com.dixa.grpc.generated.PrimeNumbersRequest result = new com.dixa.grpc.generated.PrimeNumbersRequest(this);
      result.toNumber_ = toNumber_;
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return (Builder) super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.dixa.grpc.generated.PrimeNumbersRequest) {
        return mergeFrom((com.dixa.grpc.generated.PrimeNumbersRequest)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.dixa.grpc.generated.PrimeNumbersRequest other) {
      if (other == com.dixa.grpc.generated.PrimeNumbersRequest.getDefaultInstance()) return this;
      if (other.getToNumber() != 0) {
        setToNumber(other.getToNumber());
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.dixa.grpc.generated.PrimeNumbersRequest parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.dixa.grpc.generated.PrimeNumbersRequest) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private int toNumber_ ;
    /**
     * <code>int32 toNumber = 1;</code>
     */
    public int getToNumber() {
      return toNumber_;
    }
    /**
     * <code>int32 toNumber = 1;</code>
     */
    public Builder setToNumber(int value) {
      
      toNumber_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>int32 toNumber = 1;</code>
     */
    public Builder clearToNumber() {
      
      toNumber_ = 0;
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFieldsProto3(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:com.dixa.grpc.generated.PrimeNumbersRequest)
  }

  // @@protoc_insertion_point(class_scope:com.dixa.grpc.generated.PrimeNumbersRequest)
  private static final com.dixa.grpc.generated.PrimeNumbersRequest DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.dixa.grpc.generated.PrimeNumbersRequest();
  }

  public static com.dixa.grpc.generated.PrimeNumbersRequest getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<PrimeNumbersRequest>
      PARSER = new com.google.protobuf.AbstractParser<PrimeNumbersRequest>() {
    @java.lang.Override
    public PrimeNumbersRequest parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new PrimeNumbersRequest(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<PrimeNumbersRequest> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<PrimeNumbersRequest> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public com.dixa.grpc.generated.PrimeNumbersRequest getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

