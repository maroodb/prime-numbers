package com.dixa.proxyservice.ports;

import java.util.Iterator;

public interface PrimeNumbersGateway <T> {

    Iterator<T> calculatePrimeNumbersTo(int toNumber);
}
