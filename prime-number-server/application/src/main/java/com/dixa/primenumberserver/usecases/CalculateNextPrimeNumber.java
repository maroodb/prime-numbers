package com.dixa.primenumberserver.usecases;

public class CalculateNextPrimeNumber {

    public int execute(int number) {
        int next = number + 1;
        while (!isPrime(next)) {
            next ++;
        }
        return next;

    }

    private boolean isPrime(int number) {
        if(number <= 1) {
            return false;
        }
        for (int i = 2; i * i <= number; i++) {
            if(number % i == 0) {
                return false;
            }
        }
        return true;
    }
}

