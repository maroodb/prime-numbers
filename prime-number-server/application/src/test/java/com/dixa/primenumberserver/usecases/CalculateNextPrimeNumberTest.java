package com.dixa.primenumberserver.usecases;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculateNextPrimeNumberTest {

    private final CalculateNextPrimeNumber calculateNextPrimeNumber;

    public CalculateNextPrimeNumberTest() {
        this.calculateNextPrimeNumber = new CalculateNextPrimeNumber();
    }

    @Test
    public void shouldReturnTwo() {
        int number = 1;
        int nextPrimeNumber = this.calculateNextPrimeNumber.execute(number);
        assertEquals(nextPrimeNumber, 2);
    }

    @Test
    public void shouldReturn101() {
        int number = 100;
        int nextPrimeNumber = this.calculateNextPrimeNumber.execute(number);
        assertEquals(nextPrimeNumber, 101);
    }
}
