package com.dixa.primenumberserver.server.config;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppConfig {

    private final static int DEFAULT_SERVER_PORT = 8000;
    private static AppConfig instance;
    private Properties properties;
    private AppConfig() {
        this.properties = new Properties();
        try (InputStream input = getClass().getClassLoader().getResourceAsStream("application.properties");){
            this.properties.load(input);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public int getServerPort() {
        String port = this.properties.getProperty("grpc.server.port");
        return port != null ? Integer.parseInt(port): DEFAULT_SERVER_PORT;
    }

    public static AppConfig getInstance() {
       if (instance == null) {
           instance = new AppConfig();
       }
       return instance;
    }

}
