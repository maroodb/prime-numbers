package com.dixa.primenumberserver.entrypoints.grpc;

import com.dixa.grpc.generated.NextPrimeNumbersResponse;
import com.dixa.grpc.generated.PrimeNumbersRequest;
import com.dixa.grpc.generated.PrimeNumbersServiceStreamGrpc;
import com.dixa.primenumberserver.usecases.CalculateNextPrimeNumber;
import io.grpc.stub.StreamObserver;

public class PrimeNumbersServiceStreamImpl extends PrimeNumbersServiceStreamGrpc.PrimeNumbersServiceStreamImplBase {

    private final CalculateNextPrimeNumber calculateNextPrimeNumber;

    public PrimeNumbersServiceStreamImpl() {
        this.calculateNextPrimeNumber = new CalculateNextPrimeNumber();
    }

    @Override
    public void calculatePrimeNumbers(PrimeNumbersRequest request, StreamObserver<NextPrimeNumbersResponse> responseObserver) {
        int toNumber = request.getToNumber();
        int nextPrimeNumber = 2;
        while (nextPrimeNumber <= toNumber) {
            NextPrimeNumbersResponse response = this.buildResponseFrom(nextPrimeNumber);
            responseObserver.onNext(response);
            nextPrimeNumber = this.calculateNextPrimeNumber.execute(nextPrimeNumber);
        }
        responseObserver.onCompleted();
    }

    private NextPrimeNumbersResponse buildResponseFrom(int number) {
        return NextPrimeNumbersResponse.newBuilder()
                .setNumber(number)
                .build();
    }


}
