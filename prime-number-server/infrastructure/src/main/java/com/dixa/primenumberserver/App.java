package com.dixa.primenumberserver;

import com.dixa.primenumberserver.server.GrpcServer;

import java.io.IOException;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        GrpcServer grpcServer = new GrpcServer();
            try {
                grpcServer.start(args);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }
}
