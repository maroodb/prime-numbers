package com.dixa.primenumberserver.server;

import com.dixa.primenumberserver.entrypoints.grpc.PrimeNumbersServiceStreamImpl;
import com.dixa.primenumberserver.server.config.AppConfig;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class GrpcServer {

    private final static int SERVER_PORT = AppConfig.getInstance().getServerPort();
    public void start(String [] args) throws IOException, InterruptedException {
        Server server = ServerBuilder
                .forPort(SERVER_PORT)
                .addService(new PrimeNumbersServiceStreamImpl())
                .build();
        server.start();
        System.out.println(String.format("Server started on port: %d", server.getPort()));
        server.awaitTermination();
    }
}
