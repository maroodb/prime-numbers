package com.dixa.grpc.generated;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: primeNumbers.proto")
public final class PrimeNumbersServiceStreamGrpc {

  private PrimeNumbersServiceStreamGrpc() {}

  public static final String SERVICE_NAME = "com.dixa.grpc.generated.PrimeNumbersServiceStream";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.dixa.grpc.generated.PrimeNumbersRequest,
      com.dixa.grpc.generated.NextPrimeNumbersResponse> getCalculatePrimeNumbersMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "calculatePrimeNumbers",
      requestType = com.dixa.grpc.generated.PrimeNumbersRequest.class,
      responseType = com.dixa.grpc.generated.NextPrimeNumbersResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<com.dixa.grpc.generated.PrimeNumbersRequest,
      com.dixa.grpc.generated.NextPrimeNumbersResponse> getCalculatePrimeNumbersMethod() {
    io.grpc.MethodDescriptor<com.dixa.grpc.generated.PrimeNumbersRequest, com.dixa.grpc.generated.NextPrimeNumbersResponse> getCalculatePrimeNumbersMethod;
    if ((getCalculatePrimeNumbersMethod = PrimeNumbersServiceStreamGrpc.getCalculatePrimeNumbersMethod) == null) {
      synchronized (PrimeNumbersServiceStreamGrpc.class) {
        if ((getCalculatePrimeNumbersMethod = PrimeNumbersServiceStreamGrpc.getCalculatePrimeNumbersMethod) == null) {
          PrimeNumbersServiceStreamGrpc.getCalculatePrimeNumbersMethod = getCalculatePrimeNumbersMethod = 
              io.grpc.MethodDescriptor.<com.dixa.grpc.generated.PrimeNumbersRequest, com.dixa.grpc.generated.NextPrimeNumbersResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "com.dixa.grpc.generated.PrimeNumbersServiceStream", "calculatePrimeNumbers"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.dixa.grpc.generated.PrimeNumbersRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.dixa.grpc.generated.NextPrimeNumbersResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new PrimeNumbersServiceStreamMethodDescriptorSupplier("calculatePrimeNumbers"))
                  .build();
          }
        }
     }
     return getCalculatePrimeNumbersMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static PrimeNumbersServiceStreamStub newStub(io.grpc.Channel channel) {
    return new PrimeNumbersServiceStreamStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static PrimeNumbersServiceStreamBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new PrimeNumbersServiceStreamBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static PrimeNumbersServiceStreamFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new PrimeNumbersServiceStreamFutureStub(channel);
  }

  /**
   */
  public static abstract class PrimeNumbersServiceStreamImplBase implements io.grpc.BindableService {

    /**
     */
    public void calculatePrimeNumbers(com.dixa.grpc.generated.PrimeNumbersRequest request,
        io.grpc.stub.StreamObserver<com.dixa.grpc.generated.NextPrimeNumbersResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getCalculatePrimeNumbersMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getCalculatePrimeNumbersMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                com.dixa.grpc.generated.PrimeNumbersRequest,
                com.dixa.grpc.generated.NextPrimeNumbersResponse>(
                  this, METHODID_CALCULATE_PRIME_NUMBERS)))
          .build();
    }
  }

  /**
   */
  public static final class PrimeNumbersServiceStreamStub extends io.grpc.stub.AbstractStub<PrimeNumbersServiceStreamStub> {
    private PrimeNumbersServiceStreamStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PrimeNumbersServiceStreamStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PrimeNumbersServiceStreamStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PrimeNumbersServiceStreamStub(channel, callOptions);
    }

    /**
     */
    public void calculatePrimeNumbers(com.dixa.grpc.generated.PrimeNumbersRequest request,
        io.grpc.stub.StreamObserver<com.dixa.grpc.generated.NextPrimeNumbersResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getCalculatePrimeNumbersMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class PrimeNumbersServiceStreamBlockingStub extends io.grpc.stub.AbstractStub<PrimeNumbersServiceStreamBlockingStub> {
    private PrimeNumbersServiceStreamBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PrimeNumbersServiceStreamBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PrimeNumbersServiceStreamBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PrimeNumbersServiceStreamBlockingStub(channel, callOptions);
    }

    /**
     */
    public java.util.Iterator<com.dixa.grpc.generated.NextPrimeNumbersResponse> calculatePrimeNumbers(
        com.dixa.grpc.generated.PrimeNumbersRequest request) {
      return blockingServerStreamingCall(
          getChannel(), getCalculatePrimeNumbersMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class PrimeNumbersServiceStreamFutureStub extends io.grpc.stub.AbstractStub<PrimeNumbersServiceStreamFutureStub> {
    private PrimeNumbersServiceStreamFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PrimeNumbersServiceStreamFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PrimeNumbersServiceStreamFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PrimeNumbersServiceStreamFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_CALCULATE_PRIME_NUMBERS = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final PrimeNumbersServiceStreamImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(PrimeNumbersServiceStreamImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CALCULATE_PRIME_NUMBERS:
          serviceImpl.calculatePrimeNumbers((com.dixa.grpc.generated.PrimeNumbersRequest) request,
              (io.grpc.stub.StreamObserver<com.dixa.grpc.generated.NextPrimeNumbersResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class PrimeNumbersServiceStreamBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    PrimeNumbersServiceStreamBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.dixa.grpc.generated.PrimeNumbers.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("PrimeNumbersServiceStream");
    }
  }

  private static final class PrimeNumbersServiceStreamFileDescriptorSupplier
      extends PrimeNumbersServiceStreamBaseDescriptorSupplier {
    PrimeNumbersServiceStreamFileDescriptorSupplier() {}
  }

  private static final class PrimeNumbersServiceStreamMethodDescriptorSupplier
      extends PrimeNumbersServiceStreamBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    PrimeNumbersServiceStreamMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (PrimeNumbersServiceStreamGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new PrimeNumbersServiceStreamFileDescriptorSupplier())
              .addMethod(getCalculatePrimeNumbersMethod())
              .build();
        }
      }
    }
    return result;
  }
}
